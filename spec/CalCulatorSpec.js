describe("codecalc.js",function(){
	var obl = require('../src/codecalc.js');
	
	describe("obliczanie(op,l1,l2)",function(){

		it("Dodawanie",function(){
			expect(obl.obliczanie("+", 2, 2)).toEqual(4);
		});
		
		// Tutaj nowe testy są wymagane
		it("mnożenie przez 0",function(){
			expect(obl.obliczanie("*", 0, 7)).toEqual(0);
		});
		
		it("dzielenie ujemne",function(){
			expect(obl.obliczanie("/", -2, 4)).toEqual(-0.5);
		});
		
		it("odejmowanie",function(){
			expect(obl.obliczanie("-", 4509, 10000)).toEqual(-5491);
		});
		
		it("mnożenie float",function(){
			expect(obl.obliczanie("*", 36.6, 38.9)).toEqual(1423.74);
		});
		
		it("odejmowanie float",function(){
			expect(obl.obliczanie("-", -45.09, -52.77)).toEqual(7.68);
		});
		
		it("dodawanie float",function(){
			expect(obl.obliczanie("+", 0.0001, 0.001)).toEqual(0.0011);
		});
	});
});